package com.example.aswin.booksencyclopedia;

public class Book {
    public String title;
    public String author;
    public double price;
    public String thumbnail;
}
