package com.example.aswin.booksencyclopedia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Book> listBooks;
    RecyclerView rvBooks;
    BooksAdapter adapter;

    RequestQueue queue;

    public static final String BASE_URL = "https://itunes.apple.com/search";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listBooks = new ArrayList<>();

//        Book book1 = new Book();
//        book1.title = "Beginning Android Application Development";
//        book1.author = "Wei-Meng Lee";
//        book1.price = 40.5;
//        book1.thumbnail = R.drawable.book1;
//
//        Book book2 = new Book();
//        book2.title = "Software Engineering: A Practitioner's Approach";
//        book2.author = "Roger S. Pressman";
//        book2.price = 80.9;
//        book2.thumbnail = R.drawable.book2;

//        listBooks.add(book1);
//        listBooks.add(book2);

        adapter = new BooksAdapter(this, listBooks);

        rvBooks = findViewById(R.id.rvBooks);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvBooks.setLayoutManager(linearLayoutManager);
        rvBooks.setAdapter(adapter);

        queue = Volley.newRequestQueue(this);
        queue.add(getBooks("harry potter"));
    }

    JsonObjectRequest getBooks(String term) {
        Uri uri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("term", term)
                .appendQueryParameter("media", "ebook")
                .build();

        String url = uri.toString();
//        String url = BASE_URL + "?term=" + term + "&media=ebook";

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray arrResults = response.getJSONArray("results");
                    for(int i=0; i<arrResults.length(); i++) {
                        JSONObject objResult = arrResults.getJSONObject(i);
                        Book book = new Book();
                        book.title = objResult.getString("trackName");
                        book.author = objResult.getString("artistName");
                        book.price = objResult.getDouble("price");
                        book.thumbnail = objResult.getString("artworkUrl60");

                        listBooks.add(book);
                    }

                    adapter.setListBooks(listBooks);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("MainActivity", error.getLocalizedMessage());
            }
        });

        return req;
    }
}
